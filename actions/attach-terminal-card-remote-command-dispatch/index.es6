module.exports = function(task) {

  task.widget._executeCommand = function(o, resolve, reject) {

    // Construct a packet WITH REPLY
    let packet = {
      meta: {
        cmd: o.cmd,
        reply: task.widget.util.guid()
      },
      data: o.data
    };

    let gotResponse = false;

    let timeoutId = null;

    // use timeout value from command it self
    let timeoutValue = parseInt(o.data.options.timeout) || task.widget.option("commandTimeout") || 3333;

    if((o) && (o.cmd == 'sleep')){ if(o.data.settings[0]){ timeoutValue = parseInt(o.data.settings[0])*1000 } }
    if((o) && (o.cmd == 'echo')){ if(o.data.options.pause){ timeoutValue = parseInt(o.data.options.pause)*1000 } }

    // this is the packet response
    $(document).on(packet.meta.reply, (event, incoming) => {

      let note = `terminal: [${o.cmd.replace(/[^a-z0-9_-]/g,'_')}]: [${JSON.stringify(incoming.data).replace(/</g,'&lt;').replace(/>/g,'&gt;') }]`;

      note = note.replace(/"([^"]*)"/g, function(x, y) {
        return `<span class="text-blue">${y}</span>`;
      });
      note = note.replace(/(\[|\])/g, '<span class="text-pink">$1</span>');

      task.widget.console.prepend(`<span class="text-info">${note}</span>`);

      if (o.data.options.timeout) {
        // --timeout n is on, this cancels the .off below for the catch all .timeout
        //console.log('Got Reply --timeout n is on');
      } else {
        //console.log('Got Reply --timeout n is off, clearing event listener and cancelling the timeout.');
        // got response, removing the listener.
        $(document).off(packet.meta.reply);

        if (timeoutId) clearTimeout(timeoutId);
      }

      gotResponse = true;
      resolve(incoming.data)

    });

    // this is the packet timeout
    timeoutId = setTimeout(() => {
      // it is not required to return data
      //this.console.prepend(`<div class="text-warning">Notice: timeout while waiting for response to command \`${o.cmd}\`. You may not have sufficient rights to perform this operation.</div>`);
      if (o.data.options.timeout) task.widget._echo(`<span class="text-pink">listener released (${o.data.options.timeout}ms.)</span>`);
      $(document).off(packet.meta.reply);
      resolve({})
    }, timeoutValue);

    //console.log('Command `%s` is a remote command commencing remote broadcast.', o.cmd, packet);
    $(document).trigger(o.cmd, packet);

    // at this point the command will either time out,
    // or a response packet will be captured

    // it is always a good idea to respond to commands asap

  };

  task.resolve();
};
