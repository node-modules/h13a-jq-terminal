module.exports = function(task) {
  task.widget.cliContainer = $(`
  <div class="card-text p-a-1 text-info">
    <div class="row">
      <div class="col-xs-12">
        <input style="color: green; background: black;" type="text" class="form-control form-control-cli" placeholder="%">
      </div>
    </div>
  </div>`);
  task.widget.element.append(task.widget.cliContainer);
  task.resolve();
};
