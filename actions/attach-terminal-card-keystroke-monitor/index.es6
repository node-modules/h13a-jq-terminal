module.exports = function(task) {
  task.widget.cliInputCommand = $('input.form-control-cli', task.widget.cliContainer);
  task.widget._on(task.widget.cliInputCommand, {
    "keypress": (e) => {
      if (e.keyCode == 13) {

        var str = task.widget.cliInputCommand.val();


              let commandObject = {
                meta: {
                  guid: task.widget.guid,
                  uuid: task.widget.uuid
                },
                data: str
              };
              task.widget._command_send(commandObject);

        task.widget._command_history_put(str);
        task.widget.cliInputCommand.val("");
      } else if (e.keyCode == 38) {
        task.widget.cliInputCommand.val(task.widget._command_history_get());
      }
    }
  });
  task.resolve();
};
