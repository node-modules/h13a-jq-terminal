module.exports = function(task) {
  task.widget._history_log = [];
  task.widget._history_index = 0;
  task.widget._command_history_put = function(str) {
    let lastCommand = task.widget._history_log[task.widget._history_log.length - 1];
    let lastCommandIsDifferent = lastCommand != str;

    if (str && lastCommandIsDifferent) {
      task.widget._history_log.push(str);
    } else {
      // don't put the same thing in twice
    }

    // when a string is pushed history browsing is reset
    task.widget._history_index = 0;
  };
  task.widget._command_history_get = function() {

    let theIndex = task.widget._history_log.length - 1;

    theIndex = theIndex - task.widget._history_index;

    task.widget._history_index++;

    if (theIndex < 0 || theIndex > task.widget._history_log.length - 1) {
      // things are off, just reset back to last item/0
      task.widget._history_index = 0;
      theIndex = task.widget._history_log.length - 1;
    }

    return task.widget._history_log[theIndex];
  };
  task.resolve();
};

