module.exports = function(task) {

  // NOTE: print to $('.console-position.terminal-console') as needed

  // create DOM Node for Console
  task.widget.console = $(`<div class="card-block card-text console-position terminal-console"></div>`);
  task.widget.element.append( task.widget.console );

  const trimIntervalID = setInterval(()=>{

    let size = $('.console-position.terminal-console > *').length;

    //  console.log("scrollbackSize", task.widget.option('scrollbackSize'), size)

    if(size > task.widget.option('scrollbackSize')) {
      $('.console-position.terminal-console > *').slice( task.widget.option('scrollbackSize') ).remove();
    }

  }, task.widget.option('trimInterval'));
  task.widget.destructors(()=>clearInterval(trimIntervalID));


  task.resolve();

};
