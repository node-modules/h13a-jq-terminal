const Promise = require("bluebird");

module.exports = function (task) {

  // Monitor for remote commands - all commands should be remote
  $(document).on('terminal', (event, incoming) => {

    var incomingData = incoming.data;
    if (incomingData) {

      // incoming.data can be a string, and the string can contain a pipe character, let's split it up now.
      let commands = task.widget.util.parseUnixCommand(incomingData);
      task.widget.console.prepend(`<div class="text-muted p-y-1"><i class="fa fa-binoculars"></i> Firewall intercepted incoming command packet ${JSON.stringify(incoming)} typeof ${typeof incoming.data}<hr><pre class="orange"><code>${JSON.stringify(commands, null, ' ').replace(/</g,'&lt;').replace(/>/g,'&gt;') }</code></pre></div>`);

      /*
      This is what commands object look like

      [
        {
          "cmd": "radio",
          "data": {
            "settings": [],
            "options": {
              "channel": "pressurecontrol1",
              "data": "htmlValue=7"
            }
          }
        },

        {
          "cmd": "echo",
          "data": {
            "settings": [
              "Pressure",
              "Release"
            ],
            "options": {
              "growl": true
            }
          }
        }

      ]

      */

      let promisedTask = (metadata) => {

        // make a promise

        // create resolve, reject
        let taskPromise = new Promise((resolve, reject) => {

          try {
            // execute command, sending in the resolve, reject triggers
            task.widget._executeCommand( metadata, resolve, reject );
          }
          catch (e) {
            console.log('ERROR WHILE EXECUTING COMMAND', metadata);
            console.log('ACTION ERROR', e);
            reject(e);
          }
        });

        return taskPromise;

      };

      Promise.each(commands, promisedTask).done(() => {
        // console.log("COMMANDS FINISHED", incomingData);
      });

    } // if incoming command data
  });

  // installed
  task.resolve();
};
