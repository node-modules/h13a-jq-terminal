const Factory = require('h13a-factory');

const template = require('lodash/template');
const util = require("h13a-util");

const packageJson = require("./package.json");
const packageJsonMain = require("../../package.json");

const widgetNamespace = packageJsonMain.name.toLowerCase().replace(/-/g,'');
const widgetName = packageJson.name.toLowerCase().replace(/-/g,'');
const widgetFullName = `${widgetNamespace}.${widgetName}`;

require("h13a-strategy3"); // Simple 1+2 Strategy, initialize and then display ... display ... display
const widgetIntegrationStrategy = $[widgetNamespace].h13astrategy3;

//HTML DATA
 

const flowActions = require.context("./actions/", true, /[a-z0-9-]+\/index.es6$/);

const Promise = require("bluebird");

const defaults = require("./defaults.es6");

const functions = {

    // <<YOUR FUNCTIONS HERE>>
}

const integration = $.extend({
  options: defaults,


  // * Called only once at start.
  // * Remember to call this.display(); manually
  // * Remember to take care of destructors as well.
  _prepare: function () {

    let type = this.option('type');
    let location = this.option('location');

    let factorySetup = {
      widget: this,
      info: packageJsonMain,
      card: packageJson,
      flow: this.option("flow"),
      code: flowActions,
    };

    this.factory = new Factory(factorySetup);
    this.control = this.factory.getControl({});

    this.control.run( {action: 'create'} );
    this.display();

  },

  // Step2 Notes
  // * Display On Screen
  // * This will be called each time options finish changing
  // * Called after _prepare finishes
  // * Called each time options change.
  display: function () {
  },


  _destroy: function(){
    // The public destroy() method cleans up all common data, events, etc. and then delegates out to _destroy() for custom, widget-specific, cleanup.
    this.control.run( {action: 'destroy'} );
    console.log(`${this.widgetFullName}/${this.uuid}: destructor executed.`)
  },


}, functions);

// Install Widget
$.widget(widgetFullName, widgetIntegrationStrategy, integration);
