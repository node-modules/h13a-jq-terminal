let somethingThatIsCalculated = 2 + 2;

module.exports = {

  trimInterval: 1000,
  scrollbackSize: 15,

  flow: {
    data: {
      create: {
        data: [
          { id: "setup-base-card-util-helper" },
          { id: "print-terminal-card-command-line-interface" },
          { id: "apply-base-card-command-helpers" },
          { id: "create-console" },
          { id: "attach-terminal-card-keystroke-monitor" },
          { id: "attach-terminal-card-command-history-system" },
          { id: "attach-terminal-card-remote-command-dispatch" },
          { id: "terminal-card-listen-for-remote-commands" }
        ]
      },
      destroy: {
        data: [
          { id: "terminal-card-stop-listening-for-remote-commands" },
        ]
      }
    }
  }
};
